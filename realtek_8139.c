/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

//cd /lib/modules/5.1.16-arch1-1-ARCH/build
//grep -i -n "PCI_DEVICE" $(find . | grep '\.h')

//lspci -vv -s 00:07.0
//
//ascp && make && insmod ./d_realtek_8139.ko && sleep 5 && rmmod ./d_realtek_8139.ko

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/mod_devicetable.h>
#include <linux/pci_regs.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <net/sock.h>
#include <linux/skbuff.h>
//#include <net/core/>

MODULE_LICENSE("GPL");

//////////////////////////////////////////////////////////////////////////////////////////////////////
//#define printftx(...) printf(__VA_ARGS__)
#define printftx(...)

//#define printfrx(...) printf(__VA_ARGS__)
#define printfrx(...)

#define printfd(...) printf(__VA_ARGS__)
//#define printfd(...)

//#define DEBUG
#ifdef DEBUG
	#define printf(...) printk(__VA_ARGS__)
#else
	#define printf(...)
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////
#define RTL_IDR0            0x00    // 8b
#define RTL_IDR1            0x01    // 8b
#define RTL_IDR2            0x02    // 8b
#define RTL_IDR3            0x03    // 8b
#define RTL_IDR4            0x04    // 8b
#define RTL_IDR5            0x05    // 8b
#define RTL_TSD0            0x10    // 32b
#define RTL_TSD1            0x14    // 32b
#define RTL_TSD2            0x18    // 32b
#define RTL_TSD3            0x1C    // 32b
	#define RTL_TSD_OWN         (1 << 13)   //
	#define RTL_TSD_TOK         (1 << 15)   //
	#define RTL_TSD_CHD         (1 << 28)   //
#define RTL_TSAD0           0x20    // 32b
#define RTL_TSAD1           0x24    // 32b
#define RTL_TSAD2           0x28    // 32b
#define RTL_TSAD3           0x2C    // 32b
#define RTL_RBSTART         0x30    // 32b
#define RTL_CR              0x37    // 8b
	#define RTL_CR_BUFE         (1 << 0)    //rx buffer empty
	#define RTL_CR_TE           (1 << 2)    //tx enable
	#define RTL_CR_RE           (1 << 3)    //rx enable
	#define RTL_CR_RST          (1 << 4)    //reset
#define RTL_CAPR            0x38    // 16b
#define RTL_CBR             0x3A    // 16b
#define RTL_IMR             0x3C    // 16b
	#define RTL_IMR_ROK         (1 << 0)        //rx interrupt
	#define RTL_IMR_RER         (1 << 1)        //
	#define RTL_IMR_TOK         (1 << 2)        //tx interrupt
	#define RTL_IMR_TER         (1 << 3)        //
	#define RTL_IMR_RBO         (1 << 4)        //
	#define RTL_IMR_PNU         (1 << 5)        //
	#define RTL_IMR_FOVW        (1 << 6)        //
	#define RTL_IMR_TDU         (1 << 7)        //
	#define RTL_IMR_SWI         (1 << 8)        //
	#define RTL_IMR_LeC         (1 << 13)        //
	#define RTL_IMR_TO          (1 << 14)        //
	#define RTL_IMR_SERR        (1 << 15)        //
#define RTL_ISR             0x3E    // 16b
	#define RTL_ISR_ROK         (1 << 0)    //
	#define RTL_ISR_RER         (1 << 1)    //
	#define RTL_ISR_TOK         (1 << 2)    //
	#define RTL_ISR_TER         (1 << 3)    //
	#define RTL_ISR_RBO         (1 << 4)    //
	#define RTL_ISR_PNU         (1 << 5)        //
	#define RTL_ISR_FOVW        (1 << 6)        //
	#define RTL_ISR_TDU         (1 << 7)        //
	#define RTL_ISR_SWI         (1 << 8)        //
	#define RTL_ISR_LeC         (1 << 13)        //
	#define RTL_ISR_TO          (1 << 14)        //
	#define RTL_ISR_SERR        (1 << 15)        //
#define RTL_RCR             0x44    // 32b
	#define RTL_RCR_AAP         (1 << 0)    //
	#define RTL_RCR_APM         (1 << 1)    //
	#define RTL_RCR_AM          (1 << 2)    //
	#define RTL_RCR_AB          (1 << 3)    // Accept Broadcast
	#define RTL_RCR_AR          (1 << 4)    //
	#define RTL_RCR_AER         (1 << 5)    //
	#define RTL_RCR_WRAP        (1 << 7)    //
	#define RTL_RCR_MXDMA       (0b100 << 8)    // dma 256
	//#define RTL_RCR_RBLEN       (0b11 << 11)    // buffer 64k
	#define RTL_RCR_RBLEN       (0b10 << 11)    // buffer 32k
	#define RTL_RCR_RXFTH       (0b011 << 13)    // fifo tresh 128B
	#define RTL_RCR_ERTH_S      24    //
		#define RTL_RCR_ERTH_15_16      0b1111    //
#define RTL_CONFIG0         0x51    // 8b
#define RTL_CONFIG1         0x52    // 8b
	#define RTL_CONFIG1_LWACT   (1 << 4)

///////
#define DW_ALIGN(x)         (((x) + 3) & ~3)
//////

#define rtl_rd8(x)          ioread8(rtl->rtl_addr + (x))
#define rtl_rd16(x)         ioread16(rtl->rtl_addr + (x))
#define rtl_rd32(x)         ioread32(rtl->rtl_addr + (x))

#define rtl_wr8(x, y)       iowrite8((y), rtl->rtl_addr + (x))
#define rtl_wr16(x, y)      iowrite16((y), rtl->rtl_addr + (x))
#define rtl_wr32(x, y)      iowrite32((y), rtl->rtl_addr + (x))

///////

#define rt_power_on()       iowrite8(0, rtl->rtl_addr + RTL_CONFIG1)
#define rt_power_off()      iowrite8(RTL_CONFIG1_LWACT, rtl->rtl_addr + RTL_CONFIG1)
//////////////////////////////////////////////////////////////////////////////////////////////////////

//#define RX_BUFFER_SIZE      (65536 + 16)
#define RX_BUFFER_SIZE      (32768 + 1530)
//#define RX_BUFFER_SIZE      (8192 + 1500)
#define TX_BUFFER_SIZE      1792

struct rtl {
	struct pci_dev *dev;
	struct net_device *netdev;

	void *rtl_addr;
	unsigned char *rx_buffer;
	dma_addr_t rx_buffer_dma;
	u16 rx_buffer_pointer;
	int tx_buffer_select;

	unsigned char *tx_buffer_1;
	dma_addr_t tx_buffer_dma_1;
	unsigned char *tx_buffer_2;
	dma_addr_t tx_buffer_dma_2;
	unsigned char *tx_buffer_3;
	dma_addr_t tx_buffer_dma_3;
	unsigned char *tx_buffer_4;
	dma_addr_t tx_buffer_dma_4;
};





static int rtl_init(void);
static void rtl_exit(void);
static int rtl_probe(struct pci_dev *dev, const struct pci_device_id *id);
static void rtl_remove(struct pci_dev *dev);
static int rtl_open(struct net_device *netdev);
static int rtl_stop(struct net_device *netdev);
static netdev_tx_t rtl_start_xmit(struct sk_buff *skb, struct net_device *netdev);
static enum irqreturn rtl_isr_rx(int irq, void *dev_id);
static void rtl_recieve(struct rtl *rtl);


static void rtl_soft_reset(void *rtl_addr);


//////////////////////////////////////////////////////////////////////////////////////////////////////


static const struct pci_device_id pci_rtl[] = { {
		PCI_DEVICE(PCI_VENDOR_ID_REALTEK, PCI_DEVICE_ID_REALTEK_8139),
		.driver_data = 0,
	},
	{0,}
};

static struct pci_driver pci_driver = {
	.name = "realtek_8139",
	.id_table = pci_rtl,
	.probe = rtl_probe,
	.remove = rtl_remove,
};

static struct net_device_ops netdev_ops = {
	.ndo_open = rtl_open,
	.ndo_stop = rtl_stop,
	.ndo_start_xmit = rtl_start_xmit,
};

MODULE_DEVICE_TABLE(pci, pci_rtl);
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
static int rtl_init(void)
{
	printf("\n\n\nrealtek_8139: start loading\n");

	pci_register_driver(&pci_driver);

	printf("realtek_8139: loaded\n");
	return 0;
}

static void rtl_exit(void)
{
	printf("realtek_8139: start unloading\n");

	pci_unregister_driver(&pci_driver);

	printf("realtek_8139: unloaded\n");
}


static int rtl_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
	struct net_device *netdev;
	struct rtl *rtl;
	static u32 memory;
	unsigned char *mac;
	u16 command;
	u16 vendor;
	u16 dev_ID;

	printf("realtek_8139: probe\n");

	netdev = alloc_etherdev(sizeof(struct rtl));

	netdev->netdev_ops = &netdev_ops;
	SET_NETDEV_DEV(netdev, &dev->dev);
	pci_set_drvdata(dev, netdev);

	rtl = netdev_priv(netdev);
	rtl->dev = dev;

	pci_enable_device(dev);
	pci_set_master(dev);
	//D
	pci_read_config_word(dev, PCI_VENDOR_ID, &vendor);
	pci_read_config_word(dev, PCI_DEVICE_ID, &dev_ID);
	printf("realtek_8139: vendor %X\n", vendor);
	printf("realtek_8139: dev ID %X\n", dev_ID);
	//eD

	pci_read_config_word(dev, PCI_COMMAND, &command);
	command |= PCI_COMMAND_MASTER;
	command |= PCI_COMMAND_MEMORY;
	pci_write_config_word(dev, PCI_COMMAND, command);

	//D
	pci_read_config_dword(dev, PCI_BASE_ADDRESS_0, &memory);
	printf("realtek_8139: mem0 %X  masked %lX\n", memory, memory & PCI_BASE_ADDRESS_IO_MASK);
	pci_read_config_dword(dev, PCI_BASE_ADDRESS_1, &memory);
	printf("realtek_8139: mem1 %X  masked %lX\n", memory, memory & PCI_BASE_ADDRESS_MEM_MASK);
	//eD

	rtl->rtl_addr = pci_ioremap_bar(dev, 1);
	if (!rtl->rtl_addr)
		return -1;

	printf("memory addr %p\n", rtl->rtl_addr);
	printf("rtl_mem %X", ioread32(rtl->rtl_addr));

	mac = netdev->dev_addr;
	mac[0] = rtl_rd8(RTL_IDR0);
	mac[1] = rtl_rd8(RTL_IDR1);
	mac[2] = rtl_rd8(RTL_IDR2);
	mac[3] = rtl_rd8(RTL_IDR3);
	mac[4] = rtl_rd8(RTL_IDR4);
	mac[5] = rtl_rd8(RTL_IDR5);

	register_netdev(netdev);
	printf("realtek_8139: probe done\n");
	return 0;
}


static void rtl_remove(struct pci_dev *dev)
{
	struct net_device *netdev = pci_get_drvdata(dev);
	struct rtl *rtl = netdev_priv(netdev);
	void *tmp_rtl_addr = rtl->rtl_addr;

	printf("realtek_8139: remove\n");

	unregister_netdev(netdev);

	iounmap(tmp_rtl_addr);

	pci_disable_device(dev);
}




static int rtl_open(struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	struct pci_dev *dev = rtl->dev;
	int irq_ret;

	printf("realtek_8139: device open %p", netdev);
	printf("rtl %p", rtl);
	printf("dev %p", dev);

	/////////////// init rtl /////////////////
	printf("realtek_8139: device wake");
	rt_power_on();
	rtl_soft_reset(rtl->rtl_addr);
	printf("realtek_8139: check: %X", rtl_rd8(RTL_CONFIG1));

	//////////////// interrupt ///////////////
	printf("realtek_8139: IQR %u", dev->irq);
	irq_ret = request_irq(dev->irq, rtl_isr_rx, IRQF_SHARED, "rtl_8139", rtl);
	if (!(irq_ret))
		printf("realtek_8139: IQR registered");
	else
		printf("realtek_8139: IQR NOT registered %d", irq_ret);

	dma_set_mask_and_coherent(&dev->dev, DMA_BIT_MASK(32));
	/////////////// rx buffer ////////////////
	//rx_buffer = kmalloc(RX_BUFFER_SIZE, GFP_DMA | GFP_KERNEL);
	rtl->rx_buffer = dma_alloc_coherent(&dev->dev, RX_BUFFER_SIZE, &rtl->rx_buffer_dma, GFP_KERNEL);
	memset(rtl->rx_buffer, 0, RX_BUFFER_SIZE);
	rtl_wr32(RTL_RBSTART, rtl->rx_buffer_dma);
	rtl->rx_buffer_pointer = rtl_rd16(RTL_CBR);

	/////////////// tx buffer 1 ////////////////
	rtl->tx_buffer_1 = dma_alloc_coherent(&dev->dev, TX_BUFFER_SIZE, &rtl->tx_buffer_dma_1, GFP_KERNEL);
	memset(rtl->tx_buffer_1, 0, TX_BUFFER_SIZE);
	rtl_wr32(RTL_TSAD0, rtl->tx_buffer_dma_1);
	/////////////// tx buffer 2 ////////////////
	rtl->tx_buffer_2 = dma_alloc_coherent(&dev->dev, TX_BUFFER_SIZE, &rtl->tx_buffer_dma_2, GFP_KERNEL);
	memset(rtl->tx_buffer_2, 0, TX_BUFFER_SIZE);
	rtl_wr32(RTL_TSAD1, rtl->tx_buffer_dma_2);
	/////////////// tx buffer 3 ////////////////
	rtl->tx_buffer_3 = dma_alloc_coherent(&dev->dev, TX_BUFFER_SIZE, &rtl->tx_buffer_dma_3, GFP_KERNEL);
	memset(rtl->tx_buffer_3, 0, TX_BUFFER_SIZE);
	rtl_wr32(RTL_TSAD2, rtl->tx_buffer_dma_3);
	/////////////// tx buffer 4 ////////////////
	rtl->tx_buffer_4 = dma_alloc_coherent(&dev->dev, TX_BUFFER_SIZE, &rtl->tx_buffer_dma_4, GFP_KERNEL);
	memset(rtl->tx_buffer_4, 0, TX_BUFFER_SIZE);
	rtl_wr32(RTL_TSAD3, rtl->tx_buffer_dma_4);

	if (rtl->tx_buffer_1 <= 0) {
		printf("##################################");
	}
	if (rtl->tx_buffer_2 <= 0) {
		printf("##################################2");
	}
	if (rtl->tx_buffer_3 <= 0) {
		printf("##################################3");
	}
	if (rtl->tx_buffer_4 <= 0) {
		printf("##################################4");
	}

	printf("rtl rx %llu %u", rtl->rx_buffer_dma, rtl_rd32(RTL_RBSTART));
	printf("rtl tx %llu %u", rtl->tx_buffer_dma_1, rtl_rd32(RTL_TSAD0));
	printf("rtl tx %llu %u", rtl->tx_buffer_dma_2, rtl_rd32(RTL_TSAD1));
	printf("rtl tx %llu %u", rtl->tx_buffer_dma_3, rtl_rd32(RTL_TSAD2));
	printf("rtl tx %llu %u", rtl->tx_buffer_dma_4, rtl_rd32(RTL_TSAD3));

	rtl->tx_buffer_select = 0;
	rtl->netdev = netdev;
	printf("realtek_8139: CAPR %d\n", rtl_rd16(RTL_CAPR));
	printf("realtek_8139: CBR %d\n", rtl_rd16(RTL_CBR));

	printf("realtek_8139: setting the device\n");
	rtl_wr16(RTL_IMR,
		RTL_IMR_TOK |
		RTL_IMR_ROK |
		RTL_IMR_RER |
	        RTL_IMR_TER |
	        RTL_IMR_RBO |
	        RTL_IMR_PNU |
	        RTL_IMR_FOVW |
	        RTL_IMR_TDU |
	        RTL_IMR_SWI |
	        RTL_IMR_LeC |
	        RTL_IMR_TO |
		RTL_IMR_SERR
	        );
	rtl_wr8(RTL_CR, RTL_CR_RE | RTL_CR_TE);
	rtl_wr32(RTL_RCR,
	        RTL_RCR_AAP |
	        RTL_RCR_APM |
	        RTL_RCR_AM |
	        RTL_RCR_AB |
	        RTL_RCR_WRAP |
	        RTL_RCR_MXDMA |
	        RTL_RCR_RBLEN |
	        RTL_RCR_RXFTH |
	        RTL_RCR_ERTH_15_16 << RTL_RCR_ERTH_S
	        );

	printf("realtek_8139: device open done\n");
	return 0;
}


static int rtl_stop(struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	struct pci_dev *dev = rtl->dev;

	printf("realtek_8139: device stop");

	rtl_soft_reset(rtl->rtl_addr);
	rtl_wr16(RTL_IMR, 0);
	rtl_wr32(RTL_RCR, 0);
	rtl_wr8(RTL_CR, 0);

	free_irq(dev->irq, rtl);

	rt_power_off();

	dma_free_coherent(&dev->dev, RX_BUFFER_SIZE, rtl->rx_buffer, rtl->rx_buffer_dma);
	dma_free_coherent(&dev->dev, TX_BUFFER_SIZE, rtl->tx_buffer_1, rtl->tx_buffer_dma_1);
	dma_free_coherent(&dev->dev, TX_BUFFER_SIZE, rtl->tx_buffer_2, rtl->tx_buffer_dma_2);
	dma_free_coherent(&dev->dev, TX_BUFFER_SIZE, rtl->tx_buffer_3, rtl->tx_buffer_dma_3);
	dma_free_coherent(&dev->dev, TX_BUFFER_SIZE, rtl->tx_buffer_4, rtl->tx_buffer_dma_4);

	printf("realtek_8139: device stop done");
	return 0;
}









static netdev_tx_t rtl_start_xmit(struct sk_buff *skb, struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	static u32 tx_status;
	unsigned int len = max_t(unsigned, skb->len, ETH_ZLEN);

	void *buffer;
	unsigned int tsd;
	int i = 0;

	printftx("realtek_8139: xmit %d", rtl->tx_buffer_select);
	printftx("len %d   data %d   mac %d   hdr %d", len, skb->data_len, skb->mac_len, skb->hdr_len);

	printftx("skbsend %d %p\n", rtl->tx_buffer_select, skb);
	for (i = 0; i < len; ++i) {
		printftx(KERN_CONT "%X ", skb->data[i]);
	}

	switch (rtl->tx_buffer_select) {
	case 0:
		buffer = rtl->tx_buffer_1; tsd = RTL_TSD0; rtl->tx_buffer_select = 1; break;
	case 1:
		buffer = rtl->tx_buffer_2; tsd = RTL_TSD1; rtl->tx_buffer_select = 2; break;
	case 2:
		buffer = rtl->tx_buffer_3; tsd = RTL_TSD2; rtl->tx_buffer_select = 3; break;
	case 3:
		buffer = rtl->tx_buffer_4; tsd = RTL_TSD3; rtl->tx_buffer_select = 0; break;
	default:
		rtl->tx_buffer_select = 0; printf("CHYBA"); return NETDEV_TX_OK;
	};

	tx_status = rtl_rd32(tsd);
	if (!(tx_status & RTL_TSD_OWN)) {
		printftx("BUSY");
		return NETDEV_TX_BUSY;
	}

	tx_status = len;
	memset(buffer, 0x0, TX_BUFFER_SIZE);
	skb_copy_and_csum_dev(skb, buffer);

	printftx("buffer\n");
	for (i = 0; i < len; ++i) {
		printftx(KERN_CONT "%X ", ((u8 *)buffer)[i]);
	}
	dev_kfree_skb(skb);
	//wmb();

	printftx("TSD0 %d", tx_status);
	rtl_wr32(tsd, tx_status);

	printftx("poslano %X", rtl_rd32(tsd));

	return NETDEV_TX_OK;
}



static enum irqreturn rtl_isr_rx(int irq, void *dev_id)
{
	struct rtl *rtl = dev_id;

	u16 status = rtl_rd16(RTL_ISR);
	if (status == 0)
		return IRQ_NONE;

	//printf("realtek_8139: ISR %X", status);
	if (status & RTL_ISR_ROK) {
		status &= ~RTL_ISR_ROK;
		printf("realtek_8139: ISR RTL_ISR_ROK");
		rtl_recieve(rtl);
		rtl_wr16(RTL_ISR, RTL_ISR_ROK);
	}
	if (status & RTL_ISR_RER) {
		status &= ~RTL_ISR_RER;
		printf("realtek_8139: ISR RTL_ISR_RER");
		rtl_wr16(RTL_ISR, RTL_ISR_RER);
	}
	if (status & RTL_ISR_TOK) {
		status &= ~RTL_ISR_TOK;
		printf("realtek_8139: ISR RTL_ISR_TOK");
		rtl_wr16(RTL_ISR, RTL_ISR_TOK);
	}
	if (status & RTL_ISR_TER) {
		status &= ~RTL_ISR_TER;
		printf("realtek_8139: ISR RTL_ISR_TER");
		rtl_wr16(RTL_ISR, RTL_ISR_TER);
	}
	if (status & RTL_ISR_RBO) {
		status &= ~RTL_ISR_RBO;
		printf("realtek_8139: RTL_ISR_RBO\n");
		rtl_wr16(RTL_CAPR, rtl_rd16(RTL_CBR) - 16);
		rtl_wr16(RTL_ISR, RTL_ISR_RBO);
	}
	if (status) {
		printf("realtek_8139: ISR left %X\n", status);
	}
	rtl_wr16(RTL_ISR, status);
	return IRQ_HANDLED;
}



static void rtl_recieve(struct rtl *rtl)
{
	struct sk_buff *skb;
	u16 headr = 0;
	u16 len = 0;
	u16 offset = rtl_rd16(RTL_CAPR);

	printfrx("Rx start\n");
	printfrx("realtek_8139: CAPR %d\n", offset);

	offset += 16;

	while ((rtl_rd8(RTL_CR) & RTL_CR_BUFE) == 0) {
		printfrx("!BUFFE\n");
		//printfrx("CAPR %d\n", rtl_rd16(RTL_CAPR));
		//printfrx("CBR %d\n", rtl_rd16(RTL_CBR));

		printfrx("offset %u\n", offset);
		headr = *(u16 *)(rtl->rx_buffer + offset);
		offset += 2;
		len = *(u16 *)(rtl->rx_buffer + offset);
		len -= 4;
		offset += 2;
		printfrx("head %X\n", headr);
		printfrx("len %u\n", len);

		skb = netdev_alloc_skb(rtl->netdev, len);
		skb_copy_to_linear_data(skb, rtl->rx_buffer + offset, len);
		skb_put(skb, len);
		skb->protocol = eth_type_trans(skb, rtl->netdev);
		netif_rx(skb);

		offset += len;
		offset += 4;
		offset = DW_ALIGN(offset);
		offset %= 32768;
		printfrx("offset %u\n", offset);
		/*
		//WARN_ON(offset > rtl_rd16(RTL_CBR));
		if(offset > RX_BUFFER_SIZE){
			printk(KERN_EMERG "EMERG CAPR %d\n", rtl_rd16(RTL_CAPR));
			printk(KERN_EMERG "EMERG CBR %d\n", rtl_rd16(RTL_CBR));
			printk(KERN_EMERG "EMERG len %u\n", len);
			printk(KERN_EMERG "EMERG offset %u\n", offset);
			//msleep(100);
			//BUG_ON(1);
		}
		*/
		rtl_wr16(RTL_CAPR, offset - 16);
	}
	printfrx("CAPR %d\n", rtl_rd16(RTL_CAPR));
	printfrx("Rx stop\n");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
static void rtl_soft_reset(void *rtl_addr)
{
	printf("realtek_8139: soft reset");
	iowrite8(RTL_CR_RST, rtl_addr + RTL_CR);
	while ((ioread8(rtl_addr + RTL_CR) & RTL_CR_RST) != 0) {

	}
	printf("realtek_8139: soft reset done");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////




module_init(rtl_init);
module_exit(rtl_exit);


/* crash
 crash /usr/lib/debug/lib/modules/4.18.0-115.el8.x86_64/vmlinux /var/crash/127.0.0.1-2019-07-15-17\:16\:03/vmcore
 (help mod)
 mod -s d_realtek_8139 /root/modul/realtek/d_realtek_8139.ko
 bt
 #7 [ffffad69c099f468] rtl_open at ffffffffc0749448 [d_realtek_8139]
 l *(0xffffffffc0749448)


   */
