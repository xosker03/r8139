/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

//cd /lib/modules/5.1.16-arch1-1-ARCH/build
//grep -i -n "PCI_DEVICE" $(find . | grep '\.h')

//lspci -vv -s 00:07.0
//
//ascp && make && insmod ./d_realtek_8139.ko && sleep 5 && rmmod ./d_realtek_8139.ko

#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/pci_ids.h>
#include <linux/mod_devicetable.h>
#include <linux/pci_regs.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <net/sock.h>
#include <linux/skbuff.h>
#include <asm/atomic.h>
#include <linux/ip.h>
//#include <net/core/>

MODULE_LICENSE("GPL");

//////////////////////////////////////////////////////////////////////////////////////////////////////
#define printftx(...) printf(__VA_ARGS__)
//#define printftx(...)

#define printfrx(...) printf(__VA_ARGS__)
//#define printfrx(...)

#define printfd(...) printf(__VA_ARGS__)
//#define printfd(...)

#define printfdd(...) printf(__VA_ARGS__)
//#define printfdd(...)

#define printfi(...) printf(__VA_ARGS__)
//#define printfd(...)

//#define DEBUG
#ifdef DEBUG
	#define printf(...) printk(__VA_ARGS__)
#else
	#define printf(...)
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////
#define RTL_IDR0            0x00    // 8b
#define RTL_IDR1            0x01    // 8b
#define RTL_IDR2            0x02    // 8b
#define RTL_IDR3            0x03    // 8b
#define RTL_IDR4            0x04    // 8b
#define RTL_IDR5            0x05    // 8b
#define RTL_TSD0            0x10    // 32b
#define RTL_TSD1            0x14    // 32b
#define RTL_TSD2            0x18    // 32b
#define RTL_TSD3            0x1C    // 32b
	#define RTL_TSD_OWN         (1 << 13)   //
	#define RTL_TSD_TOK         (1 << 15)   //
	#define RTL_TSD_CHD         (1 << 28)   //
#define RTL_TNPDS           0x20    // 64b
#define RTL_THPDS           0x28    // 64b
#define RTL_RBSTART         0x30    // 32b
#define RTL_CR              0x37    // 8b
	#define RTL_CR_BUFE         (1 << 0)    //rx buffer empty
	#define RTL_CR_TE           (1 << 2)    //tx enable
	#define RTL_CR_RE           (1 << 3)    //rx enable
	#define RTL_CR_RST          (1 << 4)    //reset
#define RTL_CAPR            0x38    // 16b
#define RTL_CBR             0x3A    // 16b
#define RTL_IMR             0x3C    // 16b
	#define RTL_IMR_ROK         (1 << 0)        //rx interrupt
	#define RTL_IMR_RER         (1 << 1)        //
	#define RTL_IMR_TOK         (1 << 2)        //tx interrupt
	#define RTL_IMR_TER         (1 << 3)        //
	#define RTL_IMR_RBO         (1 << 4)        //
	#define RTL_IMR_PNU         (1 << 5)        //
	#define RTL_IMR_FOVW        (1 << 6)        //
	#define RTL_IMR_TDU         (1 << 7)        //
	#define RTL_IMR_SWI         (1 << 8)        //
	#define RTL_IMR_LeC         (1 << 13)        //
	#define RTL_IMR_TO          (1 << 14)        //
	#define RTL_IMR_SERR        (1 << 15)        //
#define RTL_ISR             0x3E    // 16b
	#define RTL_ISR_ROK         (1 << 0)    //
	#define RTL_ISR_RER         (1 << 1)    //
	#define RTL_ISR_TOK         (1 << 2)    //
	#define RTL_ISR_TER         (1 << 3)    //
	#define RTL_ISR_RBO         (1 << 4)    //
	#define RTL_ISR_PNU         (1 << 5)        //
	#define RTL_ISR_FOVW        (1 << 6)        //
	#define RTL_ISR_TDU         (1 << 7)        //
	#define RTL_ISR_SWI         (1 << 8)        //
	#define RTL_ISR_LeC         (1 << 13)        //
	#define RTL_ISR_TO          (1 << 14)        //
	#define RTL_ISR_SERR        (1 << 15)        //
#define RTL_TCR             0x40    // 32b 
#define RTL_RCR             0x44    // 32b
	#define RTL_RCR_AAP         (1 << 0)    //
	#define RTL_RCR_APM         (1 << 1)    //
	#define RTL_RCR_AM          (1 << 2)    //
	#define RTL_RCR_AB          (1 << 3)    // Accept Broadcast
	#define RTL_RCR_AR          (1 << 4)    //
	#define RTL_RCR_AER         (1 << 5)    //
	#define RTL_RCR_WRAP        (1 << 7)    //
	#define RTL_RCR_MXDMA       (0b100 << 8)    // dma 256
	//#define RTL_RCR_RBLEN       (0b11 << 11)    // buffer 64k
	#define RTL_RCR_RBLEN       (0b10 << 11)    // buffer 32k
	//#define RTL_RCR_RXFTH       (0b011 << 13)    // fifo tresh 128B
	#define RTL_RCR_RXFTH       (0b100 << 13)    // fifo tresh 256B
	#define RTL_RCR_ERTH_S      24    //
		#define RTL_RCR_ERTH_15_16      0b1111    //
#define RTL_CFG9346         0x50    // 8b
#define RTL_CONFIG0         0x51    // 8b
#define RTL_CONFIG1         0x52    // 8b
	#define RTL_CONFIG1_LWACT   (1 << 4)
#define RTL_CONFIG3         0x59    // 8b 
#define RTL_MULINT          0x5C    // 16b 
#define RTL_CONFIG5         0xD8    // 8b 
#define RTL_TPPR         0xD9    // 8b 
#define RTL_CPCR            0xE0    // 16b
	#define RTL_CPCR_TE         (1 << 0)    //tx enable
	#define RTL_CPCR_RE         (1 << 1)    //rx enable
	#define RTL_CPCR_MULRW	    (1 << 3)    
	#define RTL_CPCR_DAC	    (1 << 4)    
	#define RTL_CPCR_CHCK	    (1 << 5)    
	#define RTL_CPCR_VLAN	    (1 << 6)    
#define RTL_RDSAR           0xE4    // 64b 
#define RTL_ETTHR           0xEC    // 8b 

///////
#define DW_ALIGN(x)         (((x) + 3) & ~3)
//////

#define rtl_rd8(x)          ioread8(rtl->rtl_addr + (x))
#define rtl_rd16(x)         ioread16(rtl->rtl_addr + (x))
#define rtl_rd32(x)         ioread32(rtl->rtl_addr + (x))

#define rtl_wr8(x, y)       iowrite8((y), rtl->rtl_addr + (x))
#define rtl_wr16(x, y)      iowrite16((y), rtl->rtl_addr + (x))
#define rtl_wr32(x, y)      iowrite32((y), rtl->rtl_addr + (x))


///////

#define rt_power_on()       iowrite8(0, rtl->rtl_addr + RTL_CONFIG1)
#define rt_power_off()      iowrite8(RTL_CONFIG1_LWACT, rtl->rtl_addr + RTL_CONFIG1)
//////////////////////////////////////////////////////////////////////////////////////////////////////
#define IP_PROTOCOL_TCP		0x06 
#define IP_PROTOCOL_UDP		0x11
//////////////////////////////////////////////////////////////////////////////////////////////////////
//#define RX_PACKET_SIZE		(ETH_ZLEN + 256)
#define RX_PACKET_SIZE		2048
#define RX_BUFFER_LEN		64
#define RX_BUFFER_SIZE		(sizeof(struct package) * TX_BUFFER_LEN)
#define TX_BUFFER_LEN		64
#define TX_BUFFER_SIZE		(sizeof(struct package) * TX_BUFFER_LEN)

#define RX_PID_SH	16
#define RX_PID_MSK	0b11

enum rx_pid {
	RX_NON	= 0x0,
	RX_TCP	= 0x1,
	RX_UDP	= 0x2,
	RX_IP	= 0x3,
};

#define RX_LEN_MASK	0xFFF
enum rx_sett0{
	RX_OWN	= (1 << 31),
	RX_EOR	= (1 << 30),
	RX_FS	= (1 << 29),
	RX_LS	= (1 << 28),
	RX_IPF	= (1 << 15),
	RX_UDPF = (1 << 14),
	RX_TCPF = (1 << 13),
};

#define TX_MSS_SH	16
#define TX_MSS_MSK	0b11111111111 
#define TX_LEN_MASK	0xFFFF
enum tx_sett0{
	TX_OWN	= (1 << 31),
	TX_EOR	= (1 << 30),
	TX_FS	= (1 << 29),
	TX_LS	= (1 << 28),
	TX_LGSEN= (1 << 27),
	TX_IPCS	= (1 << 18),
	TX_UDPCS= (1 << 17),
	TX_TCPCS= (1 << 16),
};

struct package {
	u32 sett0;
	u32 sett1;
	u32 low_addr;
	u32 high_addr;
};

struct stats {
	u64 rx_pack;
	u64 rx_bytes;
	u64 rx_err;
	u64 rx_drop;
	
	u64 tx_pack;
	u64 tx_bytes;
	u64 tx_err;
	u64 tx_drop;
};

struct rtl {
	struct pci_dev *dev;
	struct net_device *netdev;

	void *rtl_addr;
	
	struct package *rx_queue;
	dma_addr_t rx_queue_dma;
	struct sk_buff *rx_skb[RX_BUFFER_LEN];
	u8 rx_head;
	u8 rx_tail;

	struct package *tx_queue;
	dma_addr_t tx_queue_dma;
	struct sk_buff *tx_skb[TX_BUFFER_LEN];
	unsigned tx_head;
	unsigned tx_tail;

	struct napi_struct napi;
	struct stats stats;
};





static int rtl_init(void);
static void rtl_exit(void);
static int rtl_probe(struct pci_dev *dev, const struct pci_device_id *id);
static void rtl_remove(struct pci_dev *dev);
static int rtl_open(struct net_device *netdev);
static int rtl_stop(struct net_device *netdev);

static netdev_tx_t rtl_start_xmit(struct sk_buff *skb, struct net_device *netdev);
static enum irqreturn rtl_isr(int irq, void *dev_id);
static void rtl_transmit_clear(struct rtl *rtl);
static int rtl_tx_desc_free(struct rtl *rtl);

static void rtl_napi_schedule(struct rtl *rtl);
static int napi_poll(struct napi_struct *, int budget);
static bool rtl_rx_csum_ok(struct package *pack);
static void setup_rx_buffers(struct rtl *rtl);

static int rtl_set_features(struct net_device *dev, netdev_features_t features);
static void rtl_get_stats(struct net_device *dev,struct rtnl_link_stats64 *storage);

static void rtl_soft_reset(void *rtl_addr);


//////////////////////////////////////////////////////////////////////////////////////////////////////


static const struct pci_device_id pci_rtl[] = { {
		PCI_DEVICE(PCI_VENDOR_ID_REALTEK, PCI_DEVICE_ID_REALTEK_8139),
		.driver_data = 0,
	},
	{0,}
};

static struct pci_driver pci_driver = {
	.name = "realtek_8139",
	.id_table = pci_rtl,
	.probe = rtl_probe,
	.remove = rtl_remove,
};

static struct net_device_ops netdev_ops = {
	.ndo_open = rtl_open,
	.ndo_stop = rtl_stop,
	.ndo_start_xmit = rtl_start_xmit,
	.ndo_set_features = rtl_set_features,
	.ndo_get_stats64 = rtl_get_stats,
};

MODULE_DEVICE_TABLE(pci, pci_rtl);
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
static int rtl_init(void)
{
	printf("\n\n\nrealtek_8139: start loading\n");

	pci_register_driver(&pci_driver);

	printf("realtek_8139: loaded\n");
	return 0;
}

static void rtl_exit(void)
{
	printf("realtek_8139: start unloading\n");

	pci_unregister_driver(&pci_driver);

	printf("realtek_8139: unloaded\n");
}


static int rtl_probe(struct pci_dev *dev, const struct pci_device_id *id)
{
	struct net_device *netdev;
	struct rtl *rtl;
	static u32 memory;
	unsigned char *mac;
	u16 command;
	u16 vendor;
	u16 dev_ID;

	printf("realtek_8139: probe\n");

	netdev = alloc_etherdev(sizeof(struct rtl));

	netdev->netdev_ops = &netdev_ops;
	SET_NETDEV_DEV(netdev, &dev->dev);
	pci_set_drvdata(dev, netdev);

	rtl = netdev_priv(netdev);
	memset(rtl, 0, sizeof(struct rtl));
	rtl->dev = dev;

	pci_enable_device(dev);
	pci_set_master(dev);
	//D
	pci_read_config_word(dev, PCI_VENDOR_ID, &vendor);
	pci_read_config_word(dev, PCI_DEVICE_ID, &dev_ID);
	printf("realtek_8139: vendor %X\n", vendor);
	printf("realtek_8139: dev ID %X\n", dev_ID);
	//eD

	pci_read_config_word(dev, PCI_COMMAND, &command);
	command |= PCI_COMMAND_MASTER;
	command |= PCI_COMMAND_MEMORY;
	pci_write_config_word(dev, PCI_COMMAND, command);

	//D
	pci_read_config_dword(dev, PCI_BASE_ADDRESS_0, &memory);
	printf("realtek_8139: mem0 %X  masked %lX\n", memory, memory & PCI_BASE_ADDRESS_IO_MASK);
	pci_read_config_dword(dev, PCI_BASE_ADDRESS_1, &memory);
	printf("realtek_8139: mem1 %X  masked %lX\n", memory, memory & PCI_BASE_ADDRESS_MEM_MASK);
	//eD

	rtl->rtl_addr = pci_ioremap_bar(dev, 1);
	if (!rtl->rtl_addr)
		return -1;

	printf("memory addr %p\n", rtl->rtl_addr);
	printf("rtl_mem %X", ioread32(rtl->rtl_addr));

	mac = netdev->dev_addr;
	mac[0] = rtl_rd8(RTL_IDR0);
	mac[1] = rtl_rd8(RTL_IDR1);
	mac[2] = rtl_rd8(RTL_IDR2);
	mac[3] = rtl_rd8(RTL_IDR3);
	mac[4] = rtl_rd8(RTL_IDR4);
	mac[5] = rtl_rd8(RTL_IDR5);

	register_netdev(netdev);
	printf("realtek_8139: probe done\n");
	return 0;
}


static void rtl_remove(struct pci_dev *dev)
{
	struct net_device *netdev = pci_get_drvdata(dev);
	struct rtl *rtl = netdev_priv(netdev);
	void *tmp_rtl_addr = rtl->rtl_addr;

	printf("realtek_8139: remove\n");

	unregister_netdev(netdev);

	iounmap(tmp_rtl_addr);

	pci_disable_device(dev);
}




static int rtl_open(struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	struct pci_dev *dev = rtl->dev;
	int irq_ret;

	printf("realtek_8139: device open %p", netdev);
	printf("rtl %p", rtl);
	printf("dev %p", dev);
	
	rtl->netdev = netdev;

	/////////////// Fixed Features /////////////////
	netdev->features |= NETIF_F_IP_CSUM;
	netdev->features |= NETIF_F_TSO;
	netdev->features |= NETIF_F_GSO;
	netdev->features |= NETIF_F_SG;
	/////////////// Features /////////////////
	netdev->hw_features |= NETIF_F_IP_CSUM;
	netdev->hw_features |= NETIF_F_RXCSUM;
	netdev->hw_features |= NETIF_F_TSO;
	netdev->hw_features |= NETIF_F_GSO;
	netdev->hw_features |= NETIF_F_SG;
	/////////////// init rtl /////////////////
	printf("realtek_8139: device wake");
	rt_power_on();
	rtl_soft_reset(rtl->rtl_addr);
	printf("realtek_8139: check: %X", rtl_rd8(RTL_CONFIG1));
	rtl_wr8(RTL_CFG9346, 0xC0);
	//////////////// interrupt ///////////////
	printf("realtek_8139: IQR %u", dev->irq);
	irq_ret = request_irq(dev->irq, rtl_isr, IRQF_SHARED, "rtl_8139", rtl);
	if (!(irq_ret)) {
		printf("realtek_8139: IQR registered");
	} else {
		printf("realtek_8139: IQR NOT registered %d", irq_ret);
	}

	netif_napi_add(rtl->netdev, &rtl->napi, napi_poll, 64);
	napi_enable(&rtl->napi);
	/////////////// rx buffer ////////////////
	dma_set_mask_and_coherent(&dev->dev, DMA_BIT_MASK(64));
	rtl->rx_queue = dma_alloc_coherent(&dev->dev, RX_BUFFER_SIZE, &rtl->rx_queue_dma, GFP_KERNEL);
	if (!rtl->rx_queue) {
		printf("########################################################################");
	}
	memset(rtl->rx_queue, 0, RX_BUFFER_SIZE);
	setup_rx_buffers(rtl);

	/////////////// tx queue ////////////////
	rtl->tx_queue = dma_alloc_coherent(&dev->dev, TX_BUFFER_SIZE, &rtl->tx_queue_dma, GFP_KERNEL);
	if(!rtl->tx_queue){
		printf("########################################################################");
	}
	memset(rtl->tx_queue, 0, TX_BUFFER_SIZE);

	rtl->tx_queue[TX_BUFFER_LEN - 1].sett0 = TX_EOR;


	//printf("rtl rx %llX %X", rtl->rx_buffer_dma, rtl_rd32(RTL_RBSTART));
	//printf("rtl tx %X %X|%X", rtl->tx_queue_dma, rtl_rd32(RTL_TNPDS), rtl_rd32(RTL_TNPDS+4));

	rtl->rx_head = 0;
	rtl->rx_tail = 0;
	rtl->tx_head = 0;
	rtl->tx_tail = 0;


	printf("realtek_8139: setting the device\n");
	rtl_wr16(RTL_IMR,
		RTL_IMR_TOK |
		RTL_IMR_ROK |
		RTL_IMR_RER |
	        RTL_IMR_TER |
	        RTL_IMR_RBO |
	        RTL_IMR_PNU |
	        RTL_IMR_FOVW |
	        RTL_IMR_TDU |
	        RTL_IMR_SWI |
	        RTL_IMR_LeC |
	        RTL_IMR_TO |
		RTL_IMR_SERR
	        );
	
	rtl_wr16(RTL_CPCR, 
		RTL_CPCR_TE | 
		RTL_CPCR_RE 
		);

	rtl_wr32(RTL_TNPDS, rtl->tx_queue_dma & 0xFFFFFFFF);
	rtl_wr32(RTL_TNPDS + 4, rtl->tx_queue_dma >> 32);
	
	rtl_wr32(RTL_RDSAR, rtl->rx_queue_dma & 0xFFFFFFFF);
	rtl_wr32(RTL_RDSAR + 4, rtl->rx_queue_dma >> 32);
	
	rtl_wr32(RTL_THPDS,  0);
	rtl_wr32(RTL_THPDS + 4, 0);

	rtl_wr8(RTL_CR, RTL_CR_RE | RTL_CR_TE);
	rtl_wr32(RTL_RCR,
	        RTL_RCR_AAP |
	        RTL_RCR_APM |
	        RTL_RCR_AM |
	        RTL_RCR_AB |
	        RTL_RCR_MXDMA |
	        RTL_RCR_RXFTH |
	        0 //RTL_RCR_ERTH_15_16 << RTL_RCR_ERTH_S
	        );
	rtl_wr8(RTL_ETTHR, 0x06);
	rtl_wr32(RTL_TCR, (1 << 25) | (1 << 24) | (6 << 8));
	rtl_wr8(RTL_CONFIG1, rtl_rd8(RTL_CONFIG1) | (1 << 5));// | (1 << 0));
	rtl_wr16(RTL_MULINT, 0);

	printf("rtl rx %p %p", rtl->rx_queue, ((char *)rtl->rx_queue) + sizeof(struct package) * 64);
	printf("rtl rx %llX %X|%X", rtl->rx_queue_dma, rtl_rd32(RTL_RDSAR), rtl_rd32(RTL_RDSAR+4));
	
	printf("rtl tx %p %p", rtl->tx_queue, ((char *)rtl->tx_queue) + sizeof(struct package) * 64);
	printf("rtl tx %llX %X|%X", rtl->tx_queue_dma, rtl_rd32(RTL_TNPDS), rtl_rd32(RTL_TNPDS+4));
	printf("realtek_8139: device open done\n");
	rtl_wr8(RTL_CFG9346, 0x0);
	printf("CPCR %X", rtl_rd16(RTL_CPCR));
	return 0;
}


static int rtl_stop(struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	struct pci_dev *dev = rtl->dev;

	printf("realtek_8139: device stop");

	rtl_soft_reset(rtl->rtl_addr);
	rtl_wr16(RTL_IMR, 0);
	rtl_wr32(RTL_RCR, 0);
	rtl_wr8(RTL_CR, 0);

	free_irq(dev->irq, rtl);

	rt_power_off();

	dma_free_coherent(&dev->dev, RX_BUFFER_SIZE, rtl->rx_queue, rtl->rx_queue_dma);
	dma_free_coherent(&dev->dev, TX_BUFFER_SIZE, rtl->tx_queue, rtl->tx_queue_dma);
	
	netif_napi_del(&rtl->napi);	

	printf("realtek_8139: device stop done");
	return 0;
}









static netdev_tx_t rtl_start_xmit(struct sk_buff *skb, struct net_device *netdev)
{
	struct rtl *rtl = netdev_priv(netdev);
	u32 len_all = skb->len;
	u32 len_frag = skb->data_len;
	u16 len = len_all - len_frag;
	void *data = skb->data;
	
	struct skb_shared_info *shared_skb = NULL;
	struct skb_frag_struct *frag_skb = NULL;
	int frags_left = 0;
	int frag_counter = 0;

	struct package *pack;
	u32 *sett0;
	u64 *addr;

	u32 sett0_flags = TX_FS;
	u32 sett0_preset_flags;
		
	int tmp_tx_head = rtl->tx_head & 0x3F;
	int tmp_tx_tail = rtl->tx_tail & 0x3F;

	dma_addr_t mapping;
	
	if (rtl->tx_skb[tmp_tx_head]){
		printfdd("realtek_8139: BUSY");
		printfdd("desc status head %d  tail %d\n", tmp_tx_head, tmp_tx_tail);
		netif_stop_queue(rtl->netdev);
		return NETDEV_TX_BUSY;
	}
	if (len_frag) {
		shared_skb = skb_shinfo(skb);
		frags_left = shared_skb->nr_frags;
		if (frags_left > rtl_tx_desc_free(rtl)){
			netif_stop_queue(rtl->netdev);
			return NETDEV_TX_BUSY;
		}
	}

	printftx("realtek_8139: xmit start");

	rtl->tx_skb[tmp_tx_head] = skb;

	if (skb_is_gso(skb)){
		sett0_flags |= TX_LGSEN | (skb_shinfo(skb)->gso_size & TX_MSS_MSK) << TX_MSS_SH;
	}

	do{
		printftx("frags_left %d\n", frags_left);
		printftx("head %d  tail %d", tmp_tx_head, tmp_tx_tail);
		printftx("len %d   all %d   data %d\n", len, len_all, len_frag);
		pack = &rtl->tx_queue[tmp_tx_head];
		sett0 = &pack->sett0;
		addr = (u64 *) &pack->low_addr;

		printftx("pack %px, sett0 %px, addr %px", &rtl->tx_queue[tmp_tx_head], sett0, addr);

		mapping = dma_map_single(&rtl->dev->dev, data, len, PCI_DMA_TODEVICE);
		if (dma_mapping_error(&rtl->dev->dev, mapping)){
			printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		}

		*addr = cpu_to_le64(mapping);
		wmb();

		if (netdev->features & NETIF_F_IP_CSUM){
			if (htons(skb->protocol) == ETH_P_IP) {
				struct iphdr *ip = ip_hdr(skb);
		
				printftx("Proto: IP\n");
				sett0_flags |= TX_IPCS;

				switch (ip->protocol) {
				case IP_PROTOCOL_TCP:
					printftx("Proto: TCP\n");
					sett0_flags |= TX_TCPCS; 
					break;
				case IP_PROTOCOL_UDP: 
					printftx("Proto: UDP\n");
					sett0_flags |= TX_UDPCS; 
					break;
				default: break;
				}
			} else {

			}
		}
		sett0_flags |= len | TX_OWN | ((tmp_tx_head == TX_BUFFER_LEN - 1) ? (TX_EOR) : (0));
		if(frags_left == 0){
			sett0_flags |= TX_LS;
		}

		*sett0 = cpu_to_le32(sett0_flags);

		printftx("sett0 %X, addr %llX", rtl->tx_queue[tmp_tx_head].sett0, *addr);

		++rtl->tx_head;

		if (frags_left == 0){
			break;
		}

		frag_skb = &shared_skb->frags[frag_counter];
		len = frag_skb->size;
		//data = (char *)page_address(*((struct page **)frag_skb)) + frag_skb->page_offset;
		data = skb_frag_address(frag_skb);
		frag_counter++;
		frags_left--;
		tmp_tx_head = rtl->tx_head & 0x3F;
		
		sett0_flags = 0;
	} while(1);

	rtl_wr8(RTL_TPPR, 0b01000000);
	printftx("realtek_8139: xmit stop");
	return NETDEV_TX_OK;
}



static enum irqreturn rtl_isr(int irq, void *dev_id)
{
	struct rtl *rtl = dev_id;
	struct package *pack;
	u32 *sett0;
	int i;

	u16 status = rtl_rd16(RTL_ISR);
	if (status == 0)
		return IRQ_NONE;

	//printf("realtek_8139: ISR %X", status);
	if (status & RTL_ISR_ROK) {
		printfi("realtek_8139: ISR RTL_ISR_ROK");
		rtl_napi_schedule(rtl);
	}
	if (status & RTL_ISR_RER) {
		printfi("realtek_8139: ISR RTL_ISR_RER");
		rtl->stats.rx_err++;
		rtl->stats.rx_drop++;
		rtl_napi_schedule(rtl);
	}
	if (status & RTL_ISR_TOK) {
		printfi("realtek_8139: ISR RTL_ISR_TOK");
		rtl_napi_schedule(rtl);
	}
	if (status & RTL_ISR_TER) {
		printfi("realtek_8139: ISR RTL_ISR_TER");
		rtl->stats.tx_err++;
		rtl->stats.tx_drop++;
		rtl_napi_schedule(rtl);
	}
	if (status & RTL_ISR_RBO) {
		printfi("realtek_8139: RTL_ISR_RBO\n");
		rtl->stats.rx_drop++;
		rtl_napi_schedule(rtl);
	}
	if (status) {
		printfi("realtek_8139: ISR left %X\n", status);
	}
	rtl_wr16(RTL_ISR, status);
	return IRQ_HANDLED;
}

static void rtl_transmit_clear(struct rtl *rtl)
{
	int tmp_tx_tail = rtl->tx_tail & 0x3F;
	struct package *pack;
	u32 *sett0;
	u64 *addr;

	if(rtl->tx_head == rtl->tx_tail)
		return;
	
	netif_tx_lock(rtl->netdev);

	printfdd("desc: status head %d  tail %d\n", rtl->tx_head, rtl->tx_tail);
	while(rtl->tx_head != rtl->tx_tail){
		struct sk_buff *skb = rtl->tx_skb[tmp_tx_tail];
		pack = &rtl->tx_queue[tmp_tx_tail];
		sett0 = &pack->sett0;
		addr = (u64 *) &pack->low_addr;
		printfdd("del: %d\t%px | %X\n", tmp_tx_tail, skb, (*sett0 & (0b1111 << 28)) >> 28);
		if (*sett0 & TX_OWN)
			break;
	
		rtl->stats.tx_pack++;
		rtl->stats.tx_bytes += (*sett0) & TX_LEN_MASK;
			
		dma_unmap_single(&rtl->dev->dev, addr, (*sett0) & 0xFF, DMA_TO_DEVICE);
		if (skb){
			dev_kfree_skb_any(skb);
			rtl->tx_skb[tmp_tx_tail] = NULL;
		}
		++rtl->tx_tail;
		tmp_tx_tail = rtl->tx_tail & 0x3F;
	}
	printfdd("desc: status head %d  tail %d\n", rtl->tx_head, rtl->tx_tail);
	netif_tx_unlock(rtl->netdev);
	if (netif_queue_stopped(rtl->netdev))
		netif_wake_queue(rtl->netdev);
}

static int rtl_tx_desc_free(struct rtl *rtl)
{
	return TX_BUFFER_LEN - (rtl->tx_head - rtl->tx_tail);
}

/////////////////////////////////////////////////////////////////////

static void rtl_napi_schedule(struct rtl *rtl)
{
	printfrx("Rx start\n");
	printfrx("IMR  %X\n", rtl_rd16(RTL_IMR));

	if (napi_schedule_prep(&rtl->napi)){
		rtl_wr16(RTL_IMR, rtl_rd16(RTL_IMR) & (~(RTL_IMR_ROK | RTL_IMR_RBO )));
		wmb();
		__napi_schedule(&rtl->napi);
	}
	printfrx("Rx stop\n");
}

static int napi_poll(struct napi_struct *napi, int budget)
{
	struct rtl *rtl = container_of(napi, struct rtl, napi);
	struct sk_buff *skb;
	struct sk_buff *tmp_skb;
	dma_addr_t mapping;
	struct package *pack;
	u32 *sett0;
	u64 *addr;
	int i;
	int skip = 0;
	
	rtl_transmit_clear(rtl);
	printfrx("Napi start budget %d tail %d\n", budget, rtl->rx_tail);

	for(i = 0; i < budget; ++i){
		printfrx("Rx packet\n");
		
		skb = rtl->rx_skb[rtl->rx_tail];
		pack = &rtl->rx_queue[rtl->rx_tail];
		sett0 = &pack->sett0;
		addr = (u64 *) &pack->low_addr;
		if (*sett0 & TX_OWN)
			break;
		
		tmp_skb = netdev_alloc_skb(rtl->netdev, RX_PACKET_SIZE);
		if (!tmp_skb) {
			printfd("realtek_8139: ISR NO MEM\n");
			++skip;
			rtl->stats.rx_drop++;
			goto rx_set_desc;
		}
		rtl->stats.rx_pack++;
		rtl->stats.rx_bytes += (*sett0) & RX_LEN_MASK;

		printfrx("head %d  tail %d", rtl->rx_head, rtl->rx_tail);
		printfrx("pack %px, sett0 %px, addr %px", pack, sett0, addr);
		printfrx("sett0 %X", *sett0);

		rtl->rx_skb[rtl->rx_tail] = NULL;
		dma_unmap_single(&rtl->dev->dev, *addr, RX_PACKET_SIZE, DMA_FROM_DEVICE);
		
		skb_put(skb, *sett0 & 0xFFF);
		skb->protocol = eth_type_trans(skb, rtl->netdev);
		//Handle checksum offloading for incoming packets.
		if (rtl->netdev->features & NETIF_F_RXCSUM){
			if (rtl_rx_csum_ok(pack)){
				printfrx("CSUM_OK\n");
				skb->ip_summed = CHECKSUM_UNNECESSARY;
			} else {
				printfrx("CSUM_BAD\n");
				skb_checksum_none_assert(skb);
			}
		}
		netif_receive_skb(skb);

		skb = tmp_skb;
		rtl->rx_skb[rtl->rx_tail] = skb;
		mapping = dma_map_single(&rtl->dev->dev, skb->data, RX_PACKET_SIZE, DMA_FROM_DEVICE);
		
		*addr = cpu_to_le64(mapping);
		wmb();
rx_set_desc:
		*sett0 = RX_PACKET_SIZE | RX_OWN | ((rtl->rx_tail == RX_BUFFER_LEN - 1) ? RX_EOR : 0);
		//wmb();
		rtl->rx_tail++;
		if (rtl->rx_tail >= RX_BUFFER_LEN)
			rtl->rx_tail = 0;
		printfrx("sett0 %X", *sett0);
	}
	printfrx("Napi stop %d\n", i);

	if (i < budget){
		rtl_wr16(RTL_IMR, rtl_rd16(RTL_IMR) | RTL_IMR_ROK | RTL_IMR_RBO);
		napi_complete(napi);
		printfrx("Napi COMPLETE\n");
	}
	return i - skip;
}

static bool rtl_rx_csum_ok(struct package *pack)
{
	bool ret = true;
	u32 *sett0;
	u8 pid;

	sett0 = &pack->sett0;
	pid = (*sett0 >> RX_PID_SH) & RX_PID_MSK;

	switch (pid){
	case RX_NON: 
			ret = false;
			break;
	case RX_TCP:
			if(*sett0 & RX_TCPF)
				ret = false;
			break;
	case RX_UDP: 
			if(*sett0 & RX_UDPF)
				ret = false;
			break;
	case RX_IP:  
			if(*sett0 & RX_IPF)
				ret = false;
			break;
	}
	printfrx("PID %X, sett0 %X, ret %d\n", pid, (*sett0 >> 13) & 0b111, ret);
	return ret;
}

static void setup_rx_buffers(struct rtl *rtl)
{
	struct sk_buff *skb;
	dma_addr_t mapping;
	
	struct package *pack;
	u32 *sett0;
	u64 *addr;
	
	int i;
		
	printf("realtek_8139: Setup buffers");

	for(i = 0; i < RX_BUFFER_LEN; ++i){
		skb = netdev_alloc_skb(rtl->netdev, RX_PACKET_SIZE);
		rtl->rx_skb[i] = skb;
		mapping = dma_map_single(&rtl->dev->dev, skb->data, RX_PACKET_SIZE, DMA_FROM_DEVICE);
			
		pack = &rtl->rx_queue[i];
		sett0 = &pack->sett0;
		addr = (u64 *) &pack->low_addr;

		*addr = cpu_to_le64(mapping);
		//wmb();
		*sett0 = RX_PACKET_SIZE | RX_OWN | ((i == RX_BUFFER_LEN - 1) ? RX_EOR : 0);
		//wmb();
		
		//printfrx("head %d  tail %d\n", i, rtl->rx_tail);
		//printfrx("descriptors %px\n", rtl->rx_queue);
		//dev_info(&rtl->dev->dev, "pack %px, sett0 %px, addr %px\n", pack, sett0, addr);
		//printfrx("pack %px, sett0 %px, addr %px\n", pack, sett0, addr);
		//printfrx("sett0 %X\n", *sett0);
	}
	rtl->rx_head = 0;
	
	printf("realtek_8139: Setup buffers done");
}


static int rtl_set_features(struct net_device *netdev, netdev_features_t features)
{
	struct rtl *rtl = netdev_priv(netdev);
	
	printf("realtek_8139: Set features %llX\n", features);

	if (features & NETIF_F_RXCSUM){
		printf("realtek_8139: features rx_csum on\n");
		rtl_wr16(RTL_CPCR, rtl_rd16(RTL_CPCR) | RTL_CPCR_CHCK);
	} else {
		printf("realtek_8139: features rx_csum off\n");
		rtl_wr16(RTL_CPCR, rtl_rd16(RTL_CPCR) & (~RTL_CPCR_CHCK));
	}

	return 0;
}

static void rtl_get_stats(struct net_device *netdev,struct rtnl_link_stats64 *storage)
{
	struct rtl *rtl = netdev_priv(netdev);
	storage->rx_packets = rtl->stats.rx_pack;
	storage->rx_bytes = rtl->stats.rx_bytes;
	storage->rx_errors = rtl->stats.rx_err;
	storage->rx_dropped = rtl->stats.rx_drop;

	storage->tx_packets = rtl->stats.tx_pack;
	storage->tx_bytes = rtl->stats.tx_bytes;
	storage->tx_errors = rtl->stats.tx_err;
	storage->tx_dropped = rtl->stats.tx_drop;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
static void rtl_soft_reset(void *rtl_addr)
{
	printf("realtek_8139: soft reset");
	iowrite8(RTL_CR_RST, rtl_addr + RTL_CR);
	while ((ioread8(rtl_addr + RTL_CR) & RTL_CR_RST) != 0) {

	}
	printf("realtek_8139: soft reset done");
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////




module_init(rtl_init);
module_exit(rtl_exit);


/* crash
 crash /usr/lib/debug/lib/modules/4.18.0-115.el8.x86_64/vmlinux /var/crash/127.0.0.1-2019-07-15-17\:16\:03/vmcore
 (help mod)
 mod -s d_realtek_8139 /root/modul/realtek/d_realtek_8139.ko
 bt
 #7 [ffffad69c099f468] rtl_open at ffffffffc0749448 [d_realtek_8139]
 l *(0xffffffffc0749448)


   */
