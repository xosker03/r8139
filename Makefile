ifneq ($(KERNELRELEASE),)
# kbuild part of makefile
obj-m  := modulename.o

else
# normal makefile
    KDIR ?= /lib/modules/`uname -r`/build

default:
	$(MAKE) -C $(KDIR) M=$$PWD

clean:
	rm *.o
	rm d_realtek_8139.*
	rm modules.order
	rm Module.symvers

endif
